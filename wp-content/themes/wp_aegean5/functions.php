<?php 
	get_template_part('panel/constants');

	load_theme_textdomain( 'ci_theme', get_template_directory() . '/lang' );

	// This is the main options array. Can be accessed as a global in order to reduce function calls.
	$ci = get_option(THEME_OPTIONS);
	$ci_defaults = array();

	// The $content_width needs to be before the inclusion of the rest of the files, as it is used inside of some of them.
	if ( ! isset( $content_width ) ) $content_width = 640;

	//
	// Let's bootstrap the theme.
	//
	get_template_part('panel/bootstrap');


	//
	// Define our various image sizes.
	//
	add_theme_support( 'post-thumbnails' );
	add_image_size('ci_home_widgets', 500, 250, true );
	add_image_size('ci_home_slider', 1920, 750, true);
	add_image_size('ci_page_header', 2200, 230, true);
	add_image_size('ci_blog_featured', 630, 150, true);
	add_image_size('ci_room_2col', 450, 220, true);
	add_image_size('ci_room_slider', 920, 390, true);
	add_image_size('ci_room_related', 640, 240, true);


	add_fancybox_support();
	
	add_filter('widget_text', 'do_shortcode');
?>
