<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(); ?>

<div id="main" class="container front-container">
	<div class="row">
		<?php dynamic_sidebar('homepage'); ?>
	</div>
	
	<section class="<?php echo $content_class; ?>">
			<div class="news-lst single">

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<article class="group">
						<?php ci_the_post_thumbnail(array('class' => 'scale-with-grid')); ?>
						<div class="row">
							<div class="eleven columns alpha group">
								<h1><?php //the_title(); ?></h1>
								<?php ci_e_content(); ?>
							</div>	
						</div><!-- /row -->
						<?php //comments_template(); ?>
					</article>
				<?php endwhile; endif; ?>

			</div><!-- /news-lst -->
		</section><!-- /twelve -->
	
	
	<div class="row drop-in">
		<?php dynamic_sidebar('home-testimonials');	?>
	</div><!-- /row -->
</div><!-- /container -->

<?php get_footer(); ?>
